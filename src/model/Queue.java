package model;

import javafx.scene.layout.VBox;

import java.util.concurrent.LinkedBlockingQueue;

public class Queue implements Runnable {
    private int queueID;
    private int maxClients;
    private double waitRate;
    private int totalWait;
    private int emptyTime;
    private int clientsProcessed;
    private int elapsedTime;
    private double simulationSpeed;
    private Logger log;

    private boolean closeThread = false;

    private LinkedBlockingQueue<Client> clientList;
    public Queue(int nr, int startTime, double simSpeed, int cap, Logger log) {
        waitRate = 0;
        totalWait = 0;
        emptyTime = 0;
        clientsProcessed = 0;
        queueID = nr;
        elapsedTime = startTime;
        simulationSpeed = simSpeed;
        clientList = new LinkedBlockingQueue<>(cap);
        maxClients = cap;
        this.log = log;
    }

    public void run() {
        while ((elapsedTime <= 720 || !clientList.isEmpty()) && !closeThread) {
            if (!isEmpty()) {
                try {
                    decreaseTime();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            elapsedTime++;
            if (clientList.size() == 0) {
                emptyTime++;
            }
            try {
                Thread.sleep((long) ((11 - simulationSpeed) * 100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void addClient(Client C) {
        if (clientList.size() < maxClients) {
            clientList.add(C);
//            Platform.runLater(() -> {
//                ProgressIndicator P = new ProgressIndicator(0);
//                bindTooltip(P, new Tooltip(C.toString()));
//                visualQueue.getChildren().add(P);
//            });
        }
    }

    private void decreaseTime() throws InterruptedException {
        for (Client C : clientList) {
            C.totalWaitingTime++;
        }
        if (!clientList.peek().decreaseTime()) {
            waitRate += clientList.peek().waitRate();
            clientsProcessed++;
            Client C = clientList.take();
            totalWait += C.totalWaitingTime;

            //Platform.runLater(() -> visualQueue.getChildren().remove(0));

            //System.out.println("Client " + C + " has EXITED queue " + queueID + ".");
            log.add("Client " + C + " has EXITED queue " + queueID + ".", elapsedTime);
        }
    }

    public int emptySlots() {
        return maxClients - clientList.size();
    }
    public boolean isEmpty() {
        return clientList.isEmpty();
    }
    public int getQueueNr() {
        return queueID;
    }
    public int getProcessed() {
        return clientsProcessed;
    }
    public int getNrClients() {
        return clientList.size();
    }
    public double getAvgWait() {
        return (double) totalWait / clientsProcessed;
    }
    public LinkedBlockingQueue<Client> getClientList() {return clientList;}

    public double getSpeed() {
        double retVal = waitRate / clientsProcessed;
        if (retVal > 1) {
            return 1;
        }
        return retVal;
    }

    int getEmptyTime() {
        return emptyTime;
    }


    public void close() {
        closeThread = true;
    }

    public String toString() {
        return "QUEUE " + queueID + ": " + clientList + " COUNT: " + clientList.size() + "\nSPEED: " + waitRate / clientsProcessed + "\nPROCESSED: " + clientsProcessed + '\n';
    }
}

package model;

import java.util.Comparator;

public class ClientComparator implements Comparator<Client> {
    @Override
    public int compare(Client C1, Client C2) {
        return C1.getArrivalTime() - C2.getArrivalTime();
    }
}
